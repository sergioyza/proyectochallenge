package com.example.pruebaAzteca.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.pruebaAzteca.entity.EmployeeDataEntity;
import com.example.pruebaAzteca.entity.EmployeeWorkHoursDataEntity;
import com.example.pruebaAzteca.service.EmployeeInterface;
import com.example.pruebaAzteca.dto.EmployeeComplente;
import com.example.pruebaAzteca.dto.InicioFin;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeInterface employeeInterface;
	
	@PostMapping("add")
	public ResponseEntity<?> addEmploee(@RequestBody EmployeeComplente employeeComplente){
		return employeeInterface.addEmployee(employeeComplente);

	}
	
	@PostMapping("add-hours")
	public ResponseEntity<?> addHours(@RequestBody EmployeeWorkHoursDataEntity employeeWorkHoursDataEntity){
		return employeeInterface.addHours(employeeWorkHoursDataEntity);
	}
	
	@GetMapping("/job/{job}")
	public ResponseEntity<?> getEmployeeByJob(@PathVariable String job){
		return employeeInterface.getEmployeeByJob(job);
	}
	
	@GetMapping("/hours-worked/id/{id}/start/{inicio}/end/{fin}")
	public ResponseEntity<?> getSalaryPaid(@PathVariable String inicio,@PathVariable String fin,@PathVariable long id){
		System.out.println("controlador");
		
		Date date1=null;
		Date date2=null;
		
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(inicio);
			date2=new SimpleDateFormat("yyyy-MM-dd").parse(fin); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	
		
		InicioFin inicioFin = new InicioFin();
		inicioFin.setId(id);
		inicioFin.setFin(date1);
		inicioFin.setInicio(date2);
		System.out.println("inicioFin: "+inicioFin);
		return employeeInterface.getHoursWorked(inicioFin);
	}
	
	@GetMapping("/salary-paid/id/{id}/start/{inicio}/end/{fin}")
	public ResponseEntity<?> getHoursWorked(@PathVariable String inicio,@PathVariable String fin,@PathVariable long id){
		System.out.println("controlador");
		
		Date date1=null;
		Date date2=null;
		
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(inicio);
			date2=new SimpleDateFormat("yyyy-MM-dd").parse(fin); 
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	
		
		InicioFin inicioFin = new InicioFin();
		inicioFin.setId(id);
		inicioFin.setFin(date1);
		inicioFin.setInicio(date2);
		System.out.println("inicioFin: "+inicioFin);
		return employeeInterface.getSalaryPaid(inicioFin);
	}
}
