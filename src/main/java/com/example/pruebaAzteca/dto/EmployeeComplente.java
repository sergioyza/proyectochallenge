package com.example.pruebaAzteca.dto;

import java.util.Date;

import javax.persistence.Column;

import com.example.pruebaAzteca.entity.EmployeeDataEntity;
import com.example.pruebaAzteca.entity.GenderDataEntity;
import com.example.pruebaAzteca.entity.JobDataEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

public class EmployeeComplente {
	
	@Column(unique=true)
	private String name;
	@Column(unique=true)
	private String last_name;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date birthdate;
	
	GenderDataEntity genderDataEntity;
	JobDataEntity jobDataEntity;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public GenderDataEntity getGenderDataEntity() {
		return genderDataEntity;
	}
	public void setGenderDataEntity(GenderDataEntity genderDataEntity) {
		this.genderDataEntity = genderDataEntity;
	}
	public JobDataEntity getJobDataEntity() {
		return jobDataEntity;
	}
	public void setJobDataEntity(JobDataEntity jobDataEntity) {
		this.jobDataEntity = jobDataEntity;
	}
	@Override
	public String toString() {
		return "EmployeeComplente [name=" + name + ", last_name=" + last_name + ", birthdate=" + birthdate
				+ ", genderDataEntity=" + genderDataEntity + ", jobDataEntity=" + jobDataEntity + "]";
	}
	
	
}
