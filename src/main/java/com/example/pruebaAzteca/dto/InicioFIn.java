package com.example.pruebaAzteca.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class InicioFin {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date inicio;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date fin;
	private long id;
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "InicioFIn [inicio=" + inicio + ", fin=" + fin + ", id=" + id + "]";
	}
	
	
}
