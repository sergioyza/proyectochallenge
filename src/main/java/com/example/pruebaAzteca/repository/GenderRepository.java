package com.example.pruebaAzteca.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pruebaAzteca.entity.EmployeeDataEntity;
import com.example.pruebaAzteca.entity.GenderDataEntity;

public interface GenderRepository  extends JpaRepository<GenderDataEntity, Long>{

	GenderDataEntity findByName(String name);
}
