package com.example.pruebaAzteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pruebaAzteca.entity.JobDataEntity;

public interface JobRepository extends JpaRepository<JobDataEntity, Long>{

	JobDataEntity findByName(String name);
}
