package com.example.pruebaAzteca.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pruebaAzteca.entity.EmployeeDataEntity;


public interface EmployeeRepository extends JpaRepository<EmployeeDataEntity, Long>{

	
	ArrayList<EmployeeDataEntity> findByJobid(long user_id);
}
