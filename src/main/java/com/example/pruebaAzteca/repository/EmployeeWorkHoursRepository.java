package com.example.pruebaAzteca.repository;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.pruebaAzteca.entity.EmployeeWorkHoursDataEntity;

public interface EmployeeWorkHoursRepository  extends JpaRepository<EmployeeWorkHoursDataEntity, Long>{
	
	ArrayList<EmployeeWorkHoursDataEntity> findByWorkeddateBetweenAndEmployeeid(Date uno,Date dos,long id);
	
}
