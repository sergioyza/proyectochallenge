package com.example.pruebaAzteca.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "employees_work_hours")
public class EmployeeWorkHoursDataEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long employeeid;
	private long workedhours;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date workeddate;
	
	public long getEmployee_id() {
		return employeeid;
	}
	public void setEmployee_id(long employeeid) {
		this.employeeid = employeeid;
	}
	public long getWorked_hours() {
		return workedhours;
	}
	public void setWorked_hours(long workedhours) {
		this.workedhours = workedhours;
	}
	public Date getWorkeddate() {
		return workeddate;
	}
	public void setWorkeddate(Date workeddate) {
		this.workeddate = workeddate;
	}
	@Override
	public String toString() {
		return "employeeWorkHours [id=" + id + ", employee_id=" + employeeid + ", worked_hours=" + workedhours
				+ ", worked_date=" + workeddate + "]";
	}
	
	
	
}
