package com.example.pruebaAzteca.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "employees")
public class EmployeeDataEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	private long genderid;
	private long jobid;
	@Column(unique=true)
	private String name;
	@Column(unique=true)
	private String last_name;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date birthdate;
	public float getGender_id() {
		return genderid;
	}
	public void setGender_id(long genderid) {
		this.genderid = genderid;
	}
	public float getJob_id() {
		return jobid;
	}
	public void setJob_id(long jobid) {
		this.jobid = jobid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	@Override
	public String toString() {
		return "EmployeeDataEntity [id=" + id + ", gender_id=" + genderid + ", job_id=" + jobid + ", name=" + name
				+ ", last_name=" + last_name + ", birthdate=" + birthdate + "]";
	}
	
	

}
