package com.example.pruebaAzteca.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;

import com.example.pruebaAzteca.dto.EmployeeComplente;
import com.example.pruebaAzteca.dto.InicioFin;
import com.example.pruebaAzteca.entity.EmployeeDataEntity;
import com.example.pruebaAzteca.entity.EmployeeWorkHoursDataEntity;
import com.example.pruebaAzteca.entity.GenderDataEntity;
import com.example.pruebaAzteca.entity.JobDataEntity;
import com.example.pruebaAzteca.repository.EmployeeRepository;
import com.example.pruebaAzteca.repository.EmployeeWorkHoursRepository;
import com.example.pruebaAzteca.repository.GenderRepository;
import com.example.pruebaAzteca.repository.JobRepository;

@Service
public class EmployeeServiceImp implements EmployeeInterface {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	EmployeeWorkHoursRepository employeeWorkHoursRepository;
	@Autowired
	GenderRepository genderRepository;
	@Autowired
	JobRepository jobRepository;
	
	@Override
	public ResponseEntity<?> addEmployee(EmployeeComplente employeeComplente){
		  
		
		System.out.println("success 1 ");  
		GenderDataEntity genderDataEntity = genderRepository.findByName(employeeComplente.getGenderDataEntity().getName());
		if(genderDataEntity!=null){
			
		} else {
			genderDataEntity = genderRepository.save(employeeComplente.getGenderDataEntity());
			System.out.println("success 2");
		}  
		
		//---------------------------------------------------------------------------
		
		JobDataEntity jobDataEntity = jobRepository.findByName(employeeComplente.getJobDataEntity().getName());
		
		if(genderDataEntity!=null){
			
		} else {
		
			jobDataEntity= jobRepository.save(employeeComplente.getJobDataEntity());
			System.out.println("success");  
		}
		System.out.println("success 0");  
		System.out.println("jobDataEntity: "+jobDataEntity);
		System.out.println("genderDataEntity: "+genderDataEntity);
		EmployeeDataEntity employeeDataEntity = new EmployeeDataEntity();
		employeeDataEntity.setBirthdate(employeeComplente.getBirthdate());
		employeeDataEntity.setLast_name(employeeComplente.getLast_name());
		employeeDataEntity.setName(employeeComplente.getName());
		employeeDataEntity.setJob_id(jobDataEntity.getId());
		employeeDataEntity.setGender_id(genderDataEntity.getId());
		employeeRepository.save(employeeDataEntity);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(employeeComplente);
	}

	@Override
	public ResponseEntity<?> addHours(EmployeeWorkHoursDataEntity employeeWorkHoursDataEntity) {
		
		boolean employeeDataEntityBoolean = employeeRepository.existsById(employeeWorkHoursDataEntity.getEmployee_id());
		System.out.println("success 0"); 		
		
		if(!employeeDataEntityBoolean) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Usuario No encontrado");
		}
			
		employeeWorkHoursRepository.save(employeeWorkHoursDataEntity);
		System.out.println("success"); 
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(employeeWorkHoursDataEntity);
	}

	@Override
	public ResponseEntity<?> getEmployeeByJob(String job) {
		JobDataEntity jobDataEntity= jobRepository.findByName(job);
		
		System.out.println("jobDataEntity: "+ jobDataEntity);
		
		ArrayList<EmployeeDataEntity> ArrayEmp= employeeRepository.findByJobid(jobDataEntity.getId());
		 
		 System.out.println("ArrayEmp: "+ ArrayEmp);
		 return ResponseEntity.status(HttpStatus.ACCEPTED).body(ArrayEmp);
	}

	@Override
	public ResponseEntity<?> getHoursWorked(InicioFin inicioFIn) {
		// TODO Auto-generated method stub
		ArrayList<EmployeeWorkHoursDataEntity> arrayWorks= employeeWorkHoursRepository.findByWorkeddateBetweenAndEmployeeid(inicioFIn.getFin(),inicioFIn.getInicio(),inicioFIn.getId());
		System.out.println("buscados");
		float amountTotal= (float) arrayWorks.stream()
			      .mapToDouble(o -> o.getWorked_hours())
			      .sum();
		return ResponseEntity.status(HttpStatus.ACCEPTED).body("{\"id\": \""+inicioFIn.getId()+"\", \"total horas trabajadas\": \""+amountTotal+"\"}");
	}

	@Override
	public ResponseEntity<?> getSalaryPaid(InicioFin inicioFIn) {
		// TODO Auto-generated method stub
		return null;
	}


}
