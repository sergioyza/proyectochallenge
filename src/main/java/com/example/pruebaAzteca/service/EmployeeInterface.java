package com.example.pruebaAzteca.service;

import java.util.Date;

import org.springframework.http.ResponseEntity;

import com.example.pruebaAzteca.dto.EmployeeComplente;
import com.example.pruebaAzteca.dto.InicioFin;
import com.example.pruebaAzteca.entity.EmployeeDataEntity;
import com.example.pruebaAzteca.entity.EmployeeWorkHoursDataEntity;

public interface EmployeeInterface {
	
	public ResponseEntity<?> addEmployee(EmployeeComplente employeeComplente);
	public ResponseEntity<?> addHours(EmployeeWorkHoursDataEntity employeeWorkHoursDataEntity);
	public ResponseEntity<?> getEmployeeByJob(String job);
	public ResponseEntity<?> getHoursWorked(InicioFin inicioFIn);
	public ResponseEntity<?> getSalaryPaid(InicioFin inicioFIn);
	
}
